package cl.hardroid.meliexamen.domain.usecase

import cl.hardroid.meliexamen.UnitTest
import cl.hardroid.meliexamen.domain.base.Either
import cl.hardroid.meliexamen.domain.repository.PaymentMethodRepository
import cl.hardroid.meliexamen.fakes.FakesValuesEntity
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before
import org.junit.Test

import org.mockito.Mock

class CardIssuersUseCaseTest : UnitTest() {


    private lateinit var cardIssuerUseCase: CardIssuersUseCase
    @Mock
    private lateinit var repository: PaymentMethodRepository

    @Before
    fun setUp() {
        cardIssuerUseCase = CardIssuersUseCase(repository)
        given {
            repository.obtainPaymentMethodsCardIssuers(
                    FakesValuesEntity.ID_PAYMENT_METHOD
            )
        }.willReturn(
                Either.Right(FakesValuesEntity.getFakesCardIssuerEntityList())
        )
    }

    @Test
    fun `should get CardIssuers from PaymentRepository values` () {
        runBlocking {
            cardIssuerUseCase.run(CardIssuersUseCase.Params(FakesValuesEntity.ID_PAYMENT_METHOD))
        }

        verify(repository).obtainPaymentMethodsCardIssuers(FakesValuesEntity.ID_PAYMENT_METHOD)
        verifyNoMoreInteractions(repository)
    }
}