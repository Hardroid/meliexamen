package cl.hardroid.meliexamen.domain.usecase

import cl.hardroid.meliexamen.UnitTest
import cl.hardroid.meliexamen.domain.base.Either
import cl.hardroid.meliexamen.domain.base.UseCase
import cl.hardroid.meliexamen.domain.repository.PaymentMethodRepository
import cl.hardroid.meliexamen.fakes.FakesValuesEntity
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import kotlinx.coroutines.experimental.runBlocking

import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class PaymentMethodUseCaseTest : UnitTest(){

    lateinit var paymentMethodUseCase: PaymentMethodUseCase

    @Mock
    private lateinit var repository: PaymentMethodRepository

    @Before
    fun setUp() {
        paymentMethodUseCase = PaymentMethodUseCase(repository)
        given{
            repository.obtainPaymentMethods()
        }.willReturn(
                Either.Right(FakesValuesEntity.getFakePaymentMethodEmtityList())
        )
    }

    @Test
    fun `should get PaymentMethod from PaymentMethodRepository values` () {
        runBlocking {
            paymentMethodUseCase.run(UseCase.None())
        }

        verify(repository).obtainPaymentMethods()
        verifyNoMoreInteractions(repository)
    }
}