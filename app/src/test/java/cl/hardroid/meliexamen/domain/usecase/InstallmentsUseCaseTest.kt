package cl.hardroid.meliexamen.domain.usecase

import cl.hardroid.meliexamen.UnitTest
import cl.hardroid.meliexamen.domain.base.Either
import cl.hardroid.meliexamen.domain.base.UseCase
import cl.hardroid.meliexamen.domain.repository.PaymentMethodRepository
import cl.hardroid.meliexamen.fakes.FakesValuesEntity
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class InstallmentsUseCaseTest : UnitTest(){

    lateinit var installmentsUseCase: InstallmentsUseCase

    @Mock
    private lateinit var repository: PaymentMethodRepository

    @Before
    fun setUp() {
        installmentsUseCase = InstallmentsUseCase(repository)
        given {
            repository.obtainPaymentMethodsInstallments(
                    FakesValuesEntity.AMMOUNT,
                    FakesValuesEntity.ID_PAYMENT_METHOD,
                    FakesValuesEntity.ID_ISSUER)
        }.willReturn(
                Either.Right(FakesValuesEntity.getFakesInstallmentsList())
        )
    }

    @Test
    fun `should get Instalmments from PaymentMethodRepository values` () {
        runBlocking {
            installmentsUseCase.run(InstallmentsUseCase.Params(
                    FakesValuesEntity.ID_PAYMENT_METHOD,
                    FakesValuesEntity.ID_ISSUER,
                    FakesValuesEntity.AMMOUNT
            ))
        }

        verify(repository).obtainPaymentMethodsInstallments(
                FakesValuesEntity.AMMOUNT,
                FakesValuesEntity.ID_PAYMENT_METHOD,
                FakesValuesEntity.ID_ISSUER
        )
        verifyNoMoreInteractions(repository)
    }
}