package cl.hardroid.meliexamen.data.remote.repository

import cl.hardroid.meliexamen.UnitTest
import cl.hardroid.meliexamen.data.remote.MeliServiceApi
import cl.hardroid.meliexamen.data.remote.model.InstallmentsModel
import cl.hardroid.meliexamen.data.remote.utils.NetworkHandler
import cl.hardroid.meliexamen.domain.base.Either
import cl.hardroid.meliexamen.domain.base.Failure
import cl.hardroid.meliexamen.fakes.FakesValuesEntity
import cl.hardroid.meliexamen.fakes.FakesValuesModel
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import org.amshove.kluent.shouldBeInstanceOf
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import retrofit2.Call
import retrofit2.Response

class InstallmentsRepositoryRemoteTest : UnitTest() {

    lateinit var paymentMethodRepositoryRemote: PaymentMethodRepositoryRemote

    @Mock
    private lateinit var networkHandler: NetworkHandler
    @Mock
    private lateinit var service: MeliServiceApi
    @Mock
    private lateinit var call: Call<List<InstallmentsModel>>
    @Mock
    private lateinit var response: Response<List<InstallmentsModel>>

    @Before
    fun setUp() {
        paymentMethodRepositoryRemote = PaymentMethodRepositoryRemote(
                networkHandler,
                service
        )
    }

    @Test
    fun `should return empty installments from api sercice`() {
        given { networkHandler.isConnected }.willReturn(true)
        given { response.body() }.willReturn(emptyList())
        given { response.isSuccessful }.willReturn(true)
        given { call.execute() }.willReturn(response)
        given {
            service.getPaymentMethodsInstallments(
                    paymentMethodId = FakesValuesModel.ID_PAYMENT_METHOD,
                    amount = FakesValuesModel.AMMOUNT,
                    issuerId = FakesValuesModel.ID_ISSUER
            )
        }.willReturn(call)

        val intalmments = paymentMethodRepositoryRemote.obtainPaymentMethodsInstallments(
                FakesValuesModel.AMMOUNT,
                FakesValuesModel.ID_PAYMENT_METHOD,
                FakesValuesModel.ID_ISSUER
        )
        intalmments shouldEqual Either.Right(emptyList<InstallmentsModel>())
        verify(service).getPaymentMethodsInstallments(
                paymentMethodId = FakesValuesModel.ID_PAYMENT_METHOD,
                amount = FakesValuesModel.AMMOUNT,
                issuerId = FakesValuesModel.ID_ISSUER
        )
    }

    @Test
    fun `should return list installments from api sercice`() {
        given { networkHandler.isConnected }.willReturn(true)
        given { response.body() }.willReturn(FakesValuesModel.getFakesInstallmentsModelList())
        given { response.isSuccessful }.willReturn(true)
        given { call.execute() }.willReturn(response)
        given {
            service.getPaymentMethodsInstallments(
                    paymentMethodId = FakesValuesModel.ID_PAYMENT_METHOD,
                    amount = FakesValuesModel.AMMOUNT,
                    issuerId = FakesValuesModel.ID_ISSUER
            )
        }.willReturn(call)

        val intalmments = paymentMethodRepositoryRemote.obtainPaymentMethodsInstallments(
                FakesValuesModel.AMMOUNT,
                FakesValuesModel.ID_PAYMENT_METHOD,
                FakesValuesModel.ID_ISSUER
        )
        intalmments shouldEqual Either.Right(FakesValuesEntity.getFakesInstallmentsList())
        verify(service).getPaymentMethodsInstallments(
                paymentMethodId = FakesValuesModel.ID_PAYMENT_METHOD,
                amount = FakesValuesModel.AMMOUNT,
                issuerId = FakesValuesModel.ID_ISSUER
        )
    }

    @Test
    fun `should return no connection failure when installments does not have internet connection`() {
        given { networkHandler.isConnected }.willReturn(false)

        val intalmments = paymentMethodRepositoryRemote.obtainPaymentMethodsInstallments(
                FakesValuesModel.AMMOUNT,
                FakesValuesModel.ID_PAYMENT_METHOD,
                FakesValuesModel.ID_ISSUER
        )
        intalmments shouldBeInstanceOf Either::class.java
        intalmments.isLeft shouldEqual true
        intalmments.either({ failure -> failure shouldBeInstanceOf Failure.NetworkConnection::class.java }, {})
        verifyZeroInteractions(service)
    }

    @Test
    fun `should return server error when api installments not work`() {
        given { networkHandler.isConnected }.willReturn(true)

        val intalmments = paymentMethodRepositoryRemote.obtainPaymentMethodsInstallments(
                FakesValuesModel.AMMOUNT,
                FakesValuesModel.ID_PAYMENT_METHOD,
                FakesValuesModel.ID_ISSUER
        )
        intalmments shouldBeInstanceOf Either::class.java
        intalmments.isLeft shouldEqual true
        intalmments.either({ failure -> failure shouldBeInstanceOf Failure.ServerError::class.java }, {})
    }


}