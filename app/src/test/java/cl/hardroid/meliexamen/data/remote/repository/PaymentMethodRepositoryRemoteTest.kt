package cl.hardroid.meliexamen.data.remote.repository

import cl.hardroid.meliexamen.UnitTest
import cl.hardroid.meliexamen.data.remote.MeliServiceApi
import cl.hardroid.meliexamen.data.remote.model.PaymentMethodModel
import cl.hardroid.meliexamen.data.remote.utils.NetworkHandler
import cl.hardroid.meliexamen.domain.base.Either
import cl.hardroid.meliexamen.domain.base.Failure
import cl.hardroid.meliexamen.fakes.FakesValuesEntity
import cl.hardroid.meliexamen.fakes.FakesValuesModel
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import org.amshove.kluent.shouldBeInstanceOf
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import retrofit2.Call
import retrofit2.Response

class PaymentMethodRepositoryRemoteTest : UnitTest() {

    lateinit var paymentMethodRepositoryRemote: PaymentMethodRepositoryRemote

    @Mock
    private lateinit var networkHandler: NetworkHandler
    @Mock
    private lateinit var service: MeliServiceApi
    @Mock
    private lateinit var call: Call<List<PaymentMethodModel>>
    @Mock
    private lateinit var response: Response<List<PaymentMethodModel>>

    @Before
    fun setUp() {
        paymentMethodRepositoryRemote = PaymentMethodRepositoryRemote(
                networkHandler,
                service
        )
    }

    @Test
    fun `should return empty payment method from api sercice`() {
        given { networkHandler.isConnected }.willReturn(true)
        given { response.body() }.willReturn(emptyList())
        given { response.isSuccessful }.willReturn(true)
        given { call.execute() }.willReturn(response)
        given { service.getPaymentMethods() }.willReturn(call)

        val paymentMethodModel = paymentMethodRepositoryRemote.obtainPaymentMethods()
        paymentMethodModel shouldEqual Either.Right(emptyList<PaymentMethodModel>())
        verify(service).getPaymentMethods()
    }

    @Test
    fun `should return list payment method from api sercice`() {
        given { networkHandler.isConnected }.willReturn(true)
        given { response.body() }.willReturn(FakesValuesModel.getFakePaymentMethodModelList())
        given { response.isSuccessful }.willReturn(true)
        given { call.execute() }.willReturn(response)
        given { service.getPaymentMethods() }.willReturn(call)

        val paymentMethodModel = paymentMethodRepositoryRemote.obtainPaymentMethods()
        paymentMethodModel shouldEqual Either.Right(FakesValuesEntity.getFakePaymentMethodEmtityList())
        verify(service).getPaymentMethods()
    }

    @Test
    fun `should return no connection failure when payment method does not have internet connection`() {
        given { networkHandler.isConnected }.willReturn(false)

        val paymentMethodModel = paymentMethodRepositoryRemote.obtainPaymentMethods()
        paymentMethodModel shouldBeInstanceOf Either::class.java
        paymentMethodModel.isLeft shouldEqual true
        paymentMethodModel.either({ failure -> failure shouldBeInstanceOf Failure.NetworkConnection::class.java }, {})
        verifyZeroInteractions(service)
    }

    @Test
    fun `should return server error when api payment method not work`() {
        given { networkHandler.isConnected }.willReturn(true)

        val paymentMethodModel = paymentMethodRepositoryRemote.obtainPaymentMethods()
        paymentMethodModel shouldBeInstanceOf Either::class.java
        paymentMethodModel.isLeft shouldEqual true
        paymentMethodModel.either({ failure -> failure shouldBeInstanceOf Failure.ServerError::class.java }, {})
    }


}