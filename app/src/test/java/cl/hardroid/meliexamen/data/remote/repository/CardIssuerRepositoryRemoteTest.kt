package cl.hardroid.meliexamen.data.remote.repository

import cl.hardroid.meliexamen.UnitTest
import cl.hardroid.meliexamen.data.remote.MeliServiceApi
import cl.hardroid.meliexamen.data.remote.model.CardIssuersModel
import cl.hardroid.meliexamen.data.remote.utils.NetworkHandler
import cl.hardroid.meliexamen.domain.base.Either
import cl.hardroid.meliexamen.domain.base.Failure
import cl.hardroid.meliexamen.fakes.FakesValuesEntity
import cl.hardroid.meliexamen.fakes.FakesValuesModel
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import org.amshove.kluent.shouldBeInstanceOf
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import retrofit2.Call
import retrofit2.Response

class CardIssuerRepositoryRemoteTest : UnitTest() {

    lateinit var paymentMethodRepositoryRemote: PaymentMethodRepositoryRemote

    @Mock
    private lateinit var networkHandler: NetworkHandler
    @Mock
    private lateinit var service: MeliServiceApi
    @Mock
    private lateinit var call: Call<List<CardIssuersModel>>
    @Mock
    private lateinit var response: Response<List<CardIssuersModel>>

    @Before
    fun setUp() {
        paymentMethodRepositoryRemote = PaymentMethodRepositoryRemote(
                networkHandler,
                service
        )
    }

    @Test
    fun `should return empty card issuer from api sercice`() {
        given { networkHandler.isConnected }.willReturn(true)
        given { response.body() }.willReturn(emptyList())
        given { response.isSuccessful }.willReturn(true)
        given { call.execute() }.willReturn(response)
        given {
            service.getPaymentMethodsCardIssuers(
                    paymentMethodId = FakesValuesModel.ID_PAYMENT_METHOD
            )
        }.willReturn(call)

        val cardIssuer = paymentMethodRepositoryRemote.obtainPaymentMethodsCardIssuers(FakesValuesModel.ID_PAYMENT_METHOD)
        cardIssuer shouldEqual Either.Right(emptyList<CardIssuersModel>())
        verify(service).getPaymentMethodsCardIssuers(
                paymentMethodId = FakesValuesModel.ID_PAYMENT_METHOD
        )
    }

    @Test
    fun `should return list card issuer from api sercice`() {
        given { networkHandler.isConnected }.willReturn(true)
        given { response.body() }.willReturn(FakesValuesModel.getFakesCardIssuerEntityList())
        given { response.isSuccessful }.willReturn(true)
        given { call.execute() }.willReturn(response)
        given {
            service.getPaymentMethodsCardIssuers(
                    paymentMethodId = FakesValuesModel.ID_PAYMENT_METHOD
            )
        }.willReturn(call)

        val cardIssuer = paymentMethodRepositoryRemote.obtainPaymentMethodsCardIssuers(FakesValuesModel.ID_PAYMENT_METHOD)
        cardIssuer shouldEqual Either.Right(FakesValuesEntity.getFakesCardIssuerEntityList())
        verify(service).getPaymentMethodsCardIssuers(
                paymentMethodId = FakesValuesModel.ID_PAYMENT_METHOD
        )
    }

    @Test
    fun `should return no connection failure when card issuer does not have internet connection`() {
        given { networkHandler.isConnected }.willReturn(false)

        val cardIssuer = paymentMethodRepositoryRemote.obtainPaymentMethodsCardIssuers(FakesValuesModel.ID_PAYMENT_METHOD)
        cardIssuer shouldBeInstanceOf Either::class.java
        cardIssuer.isLeft shouldEqual true
        cardIssuer.either({ failure -> failure shouldBeInstanceOf Failure.NetworkConnection::class.java }, {})
        verifyZeroInteractions(service)
    }

    @Test
    fun `should return server error when api card issuer not work`() {
        given { networkHandler.isConnected }.willReturn(true)

        val cardIssuer = paymentMethodRepositoryRemote.obtainPaymentMethodsCardIssuers(FakesValuesModel.ID_PAYMENT_METHOD)
        cardIssuer shouldBeInstanceOf Either::class.java
        cardIssuer.isLeft shouldEqual true
        cardIssuer.either({ failure -> failure shouldBeInstanceOf Failure.ServerError::class.java }, {})
    }


}