package cl.hardroid.meliexamen.fakes

import cl.hardroid.meliexamen.domain.base.Either
import cl.hardroid.meliexamen.domain.base.Failure
import cl.hardroid.meliexamen.domain.entity.CardIssuersEntity
import cl.hardroid.meliexamen.domain.entity.InstallmentsEntity
import cl.hardroid.meliexamen.domain.entity.IssuerEntity
import cl.hardroid.meliexamen.domain.entity.PaymentMethodEntity
import cl.hardroid.meliexamen.extensions.empty

class FakesValuesEntity {


    companion object {
        val ID_PAYMENT_METHOD = "123456"
        val AMMOUNT = 5000
        val URL_FAKE_THUMBNAIL = "https://dominio.cl/"
        val ID_ISSUER = "1122"

        fun getFakesCardIssuerEntityList(): List<CardIssuersEntity> {
            val values = ArrayList<CardIssuersEntity>()
            for (i in 1..20) {
                values.add(getFakeCardIssuerEntity(i))
            }
            return values
        }

        fun getFakeCardIssuerEntity(id: Int) = CardIssuersEntity(
                id.toString(),
                "name {$id}",
                URL_FAKE_THUMBNAIL,
                URL_FAKE_THUMBNAIL,
                String.empty(),
                String.empty()
        )

        fun getFakePaymentMethodEmtityList(): List<PaymentMethodEntity> {
            val values = ArrayList<PaymentMethodEntity>()
            for (i in 1..20) {
                values.add(getFakePaymentMethodEntity(i))
            }
            return values
        }

        fun getFakePaymentMethodEntity(id: Int) = PaymentMethodEntity(
                id.toString(),
                "name {$id}",
                String.empty(),
                String.empty(),
                URL_FAKE_THUMBNAIL,
                URL_FAKE_THUMBNAIL,
                String.empty()
        )

        fun getFakesInstallmentsList(): List<InstallmentsEntity> {
            val values = ArrayList<InstallmentsEntity>()
            for (i in 1..20) {
                values.add(getFakeInstallmentsEntity(i))
            }
            return values
        }

        fun getFakeInstallmentsEntity(id: Int) = InstallmentsEntity(
                id.toString(),
                id.toString(),
                getFakeIssuerEntity(id),
                String.empty(),
                String.empty(),
                emptyList()
        )

        fun getFakeIssuerEntity(id: Int) = IssuerEntity(
                id.toString(),
                "name {$id}",
                URL_FAKE_THUMBNAIL,
                URL_FAKE_THUMBNAIL
        )
    }
}