package cl.hardroid.meliexamen.fakes

import cl.hardroid.meliexamen.data.remote.model.CardIssuersModel
import cl.hardroid.meliexamen.data.remote.model.InstallmentsModel
import cl.hardroid.meliexamen.data.remote.model.IssuerModel
import cl.hardroid.meliexamen.data.remote.model.PaymentMethodModel
import cl.hardroid.meliexamen.extensions.empty

class FakesValuesModel {


    companion object {
        val ID_PAYMENT_METHOD = "123456"
        val AMMOUNT = 5000
        val URL_FAKE_THUMBNAIL = "https://dominio.cl/"
        val ID_ISSUER = "1122"

        fun getFakesCardIssuerEntityList(): List<CardIssuersModel> {
            val values = ArrayList<CardIssuersModel>()
            for (i in 1..20) {
                values.add(getFakeCardIssuerModel(i))
            }
            return values
        }

        fun getFakeCardIssuerModel(id: Int) = CardIssuersModel(
                id.toString(),
                "name {$id}",
                URL_FAKE_THUMBNAIL,
                URL_FAKE_THUMBNAIL,
                String.empty(),
                String.empty()
        )

        fun getFakePaymentMethodModelList(): List<PaymentMethodModel> {
            val values = ArrayList<PaymentMethodModel>()
            for (i in 1..20) {
                values.add(getFakePaymentMethodModel(i))
            }
            return values
        }

        fun getFakePaymentMethodModel(id: Int) = PaymentMethodModel(
                id.toString(),
                "name {$id}",
                String.empty(),
                String.empty(),
                URL_FAKE_THUMBNAIL,
                URL_FAKE_THUMBNAIL,
                String.empty()
        )

        fun getFakesInstallmentsModelList(): List<InstallmentsModel> {
            val values = ArrayList<InstallmentsModel>()
            for (i in 1..20) {
                values.add(getFakesInstallmentsModel(i))
            }
            return values
        }

        fun getFakesInstallmentsModel(id: Int) = InstallmentsModel(
                id.toString(),
                id.toString(),
                getFakeIssuerModel(id),
                String.empty(),
                String.empty(),
                emptyList()
        )

        fun getFakeIssuerModel(id: Int) = IssuerModel(
                id.toString(),
                "name {$id}",
                URL_FAKE_THUMBNAIL,
                URL_FAKE_THUMBNAIL
        )
    }
}