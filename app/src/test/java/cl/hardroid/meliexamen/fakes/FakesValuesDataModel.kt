package cl.hardroid.meliexamen.fakes

import cl.hardroid.meliexamen.domain.base.Either
import cl.hardroid.meliexamen.domain.base.Failure
import cl.hardroid.meliexamen.domain.entity.CardIssuersEntity
import cl.hardroid.meliexamen.domain.entity.InstallmentsEntity
import cl.hardroid.meliexamen.domain.entity.IssuerEntity
import cl.hardroid.meliexamen.domain.entity.PaymentMethodEntity
import cl.hardroid.meliexamen.extensions.empty
import cl.hardroid.meliexamen.presentation.cardissuers.CardIssuersDataModel
import cl.hardroid.meliexamen.presentation.installments.InstallmentsDataModel
import cl.hardroid.meliexamen.presentation.installments.IssuerDataModel
import cl.hardroid.meliexamen.presentation.paymentmethod.PaymentMethodDataModel

class FakesValuesDataModel {


    companion object {
        val ID_PAYMENT_METHOD = "123456"
        val AMMOUNT = 5000
        val URL_FAKE_THUMBNAIL = "https://dominio.cl/"
        val ID_ISSUER = "1122"

        fun getFakesCardIssuerDataModelList(): List<CardIssuersDataModel> {
            val values = ArrayList<CardIssuersDataModel>()
            for (i in 1..20) {
                values.add(getFakeCardIssuerDataModel(i))
            }
            return values
        }

        fun getFakeCardIssuerDataModel(id: Int) = CardIssuersDataModel(
                id.toString(),
                "name {$id}",
                URL_FAKE_THUMBNAIL,
                URL_FAKE_THUMBNAIL,
                String.empty(),
                String.empty()
        )

        fun getFakePaymentMethodDataModelList(): List<PaymentMethodDataModel> {
            val values = ArrayList<PaymentMethodDataModel>()
            for (i in 1..20) {
                values.add(getFakePaymentMethodDataModel(i))
            }
            return values
        }

        fun getFakePaymentMethodDataModel(id: Int) = PaymentMethodDataModel(
                id.toString(),
                "name {$id}",
                String.empty(),
                String.empty(),
                URL_FAKE_THUMBNAIL,
                URL_FAKE_THUMBNAIL,
                String.empty()
        )

        fun getFakesInstallmentsDataModelList(): List<InstallmentsDataModel> {
            val values = ArrayList<InstallmentsDataModel>()
            for (i in 1..20) {
                values.add(getFakesInstallmentsDataModel(i))
            }
            return values
        }

        fun getFakesInstallmentsDataModel(id: Int) = InstallmentsDataModel(
                id.toString(),
                id.toString(),
                getFakeIssuerDataModel(id),
                String.empty(),
                String.empty(),
                emptyList()
        )

        fun getFakeIssuerDataModel(id: Int) = IssuerDataModel(
                id.toString(),
                "name {$id}",
                URL_FAKE_THUMBNAIL,
                URL_FAKE_THUMBNAIL
        )
    }
}