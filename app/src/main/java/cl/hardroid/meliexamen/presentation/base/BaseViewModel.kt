package cl.hardroid.meliexamen.presentation.base

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import cl.hardroid.meliexamen.domain.base.Failure

open class BaseViewModel : ViewModel(){

    var failure: MutableLiveData<Failure> = MutableLiveData()

    open fun handleFailure(failure: Failure) {
        this.failure.value = failure
        this.failure.postValue(
                failure
        )
    }
}