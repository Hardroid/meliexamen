package cl.hardroid.meliexamen.presentation.installments

interface InstallmentsNavigator {
    fun goToPayerCost(payerCostsDataModel: PayerCostsDataModel)
}