package cl.hardroid.meliexamen.presentation.paymentmethod

interface PaymentMethodNavigator {
    fun goToSeleccionarBanco(idMedioDePago: PaymentMethodDataModel?)
}