package cl.hardroid.meliexamen.presentation.cardissuers

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import cl.hardroid.meliexamen.R
import cl.hardroid.meliexamen.extensions.inflate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_payment_method.view.*

class CardIssuersAdapter : RecyclerView.Adapter<CardIssuersAdapter.MedioDePagoViewHolder>() {

    lateinit var listCardIssuersDataModel: List<CardIssuersDataModel>
    lateinit var navigator: CardIssuersNavigator

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MedioDePagoViewHolder(parent.inflate(R.layout.row_payment_method))

    override fun getItemCount() = listCardIssuersDataModel.size

    override fun onBindViewHolder(holder: MedioDePagoViewHolder, position: Int) =
            holder.bind(listCardIssuersDataModel[position], navigator)

    class MedioDePagoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(cardIssuersDataModel: CardIssuersDataModel, navigator: CardIssuersNavigator) {
            itemView.txtPaymentMethod.text = cardIssuersDataModel.name
            itemView.txtPaymentMethod.setOnClickListener {
                navigator.goToInstallments(cardIssuersDataModel)
            }
            Glide.with(itemView.context).load(cardIssuersDataModel.thumbnail).into(itemView.imgThumbnail)

        }
    }
}