package cl.hardroid.meliexamen.presentation.paymentmethod

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import cl.hardroid.meliexamen.extensions.toViewDataModel
import cl.hardroid.meliexamen.domain.base.UseCase
import cl.hardroid.meliexamen.domain.entity.PaymentMethodEntity
import cl.hardroid.meliexamen.domain.usecase.PaymentMethodUseCase
import cl.hardroid.meliexamen.presentation.base.BaseViewModel
import javax.inject.Inject

class PaymentMethodViewModel @Inject constructor(
        private val paymentMethodUseCase: PaymentMethodUseCase
) : BaseViewModel() {

    var paymentMethodDataModelList:
            MutableLiveData<List<PaymentMethodDataModel>> = MutableLiveData()

    fun getPaymentMethod(): LiveData<List<PaymentMethodDataModel>> {
        return paymentMethodDataModelList
    }

    fun loadPaymentMethodDataModel() = paymentMethodUseCase(UseCase.None()) {
        it.either(::handleFailure, ::handleListPaymentsMethods)
    }

    private fun handleListPaymentsMethods(list: List<PaymentMethodEntity>) {
        paymentMethodDataModelList.value = list.map { it.toViewDataModel(it) }
        paymentMethodDataModelList.postValue(
                list.map { it.toViewDataModel(it) }
        )
    }


}