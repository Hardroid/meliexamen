package cl.hardroid.meliexamen.presentation.installments

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import cl.hardroid.meliexamen.domain.entity.InstallmentsEntity
import cl.hardroid.meliexamen.domain.usecase.InstallmentsUseCase
import cl.hardroid.meliexamen.extensions.toViewDataModel
import cl.hardroid.meliexamen.presentation.base.BaseViewModel
import javax.inject.Inject

class InstallmentsViewModel @Inject constructor(
        private val installmentsUseCase: InstallmentsUseCase
) : BaseViewModel() {

    var paymentMethodDataModelList:
            MutableLiveData<InstallmentsDataModel> = MutableLiveData()

    fun getInstallment(): LiveData<InstallmentsDataModel> {
        return paymentMethodDataModelList
    }

    fun loadPaymentMethodDataModel(paymentId: String, cardIssuerId: String, ammount: Int) = installmentsUseCase(InstallmentsUseCase.Params(paymentId, cardIssuerId, ammount)) {
        it.either(::handleFailure, ::handleInstallments)
    }

    private fun handleInstallments(installmentsEntity: List<InstallmentsEntity>) {
        paymentMethodDataModelList.value = installmentsEntity.get(0).toViewDataModel(installmentsEntity.get(0))
        paymentMethodDataModelList.postValue(
                paymentMethodDataModelList.value
        )
    }


}