package cl.hardroid.meliexamen.presentation.cardissuers

interface CardIssuersNavigator {
    fun goToInstallments(idMedioDePago: CardIssuersDataModel?)
}