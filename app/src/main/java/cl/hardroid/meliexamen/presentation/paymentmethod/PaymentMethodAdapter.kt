package cl.hardroid.meliexamen.presentation.paymentmethod

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import cl.hardroid.meliexamen.R
import cl.hardroid.meliexamen.extensions.inflate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_payment_method.view.*

class PaymentMethodAdapter : RecyclerView.Adapter<PaymentMethodAdapter.MedioDePagoViewHolder>() {

    lateinit var listMedios: List<PaymentMethodDataModel>
    lateinit var navigator: PaymentMethodNavigator

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MedioDePagoViewHolder(parent.inflate(R.layout.row_payment_method))

    override fun getItemCount() = listMedios.size

    override fun onBindViewHolder(holder: MedioDePagoViewHolder, position: Int) =
            holder.bind(listMedios[position], navigator)

    class MedioDePagoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(paymentMethodDataModel: PaymentMethodDataModel, navigator: PaymentMethodNavigator) {
            itemView.txtPaymentMethod.text = paymentMethodDataModel.name
            itemView.txtPaymentMethod.setOnClickListener {
                navigator.goToSeleccionarBanco(paymentMethodDataModel)
            }
            Glide.with(itemView.context).load(paymentMethodDataModel.thumbnail).into(itemView.imgThumbnail)

        }
    }
}