package cl.hardroid.meliexamen.presentation.cardissuers

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import cl.hardroid.meliexamen.domain.base.UseCase
import cl.hardroid.meliexamen.domain.entity.CardIssuersEntity
import cl.hardroid.meliexamen.domain.entity.PaymentMethodEntity
import cl.hardroid.meliexamen.domain.usecase.CardIssuersUseCase
import cl.hardroid.meliexamen.extensions.toViewDataModel
import cl.hardroid.meliexamen.presentation.base.BaseViewModel
import javax.inject.Inject

class CardIssuersViewModel @Inject constructor(
        private val cardIssuersUseCase: CardIssuersUseCase
) : BaseViewModel() {

    var cardIssuersDataModelList:
            MutableLiveData<List<CardIssuersDataModel>> = MutableLiveData()

    fun getCardIssuerd(): LiveData<List<CardIssuersDataModel>> {
        return cardIssuersDataModelList
    }

    fun loadCardIssuerd(id: String?) = cardIssuersUseCase(CardIssuersUseCase.Params(id)) {
        it.either(::handleFailure, ::handleCardIssuers)
    }

    private fun handleCardIssuers(list: List<CardIssuersEntity>) {
        cardIssuersDataModelList.value = list.map { it.toViewDataModel(it) }
        cardIssuersDataModelList.postValue(
                cardIssuersDataModelList.value
        )
    }


}