package cl.hardroid.meliexamen.presentation.installments


data class PayerCostsDataModel(
        val installments: Int?,
        val installmentRate: Float?,
        val discountRate: Int?,
        val labels: List<String>?,
        val installmentRateCollector: List<String>?,
        val minAllowedAmount: Int?,
        val maxAllowedAmount: Int?,
        val recommendedMessage: String?,
        val installmentAmount: Float?,
        val totalAmount: Float?
)