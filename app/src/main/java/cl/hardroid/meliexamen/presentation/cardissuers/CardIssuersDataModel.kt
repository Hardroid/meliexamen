package cl.hardroid.meliexamen.presentation.cardissuers


data class CardIssuersDataModel(
        val id: String,
        val name: String,
        val secureThumbnail: String?,
        val thumbnail: String?,
        val processingMode: String?,
        val merchantAccountId: String?
)