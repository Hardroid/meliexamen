package cl.hardroid.meliexamen.presentation.cardissuers

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import cl.hardroid.meliexamen.R
import cl.hardroid.meliexamen.domain.base.Failure
import cl.hardroid.meliexamen.extensions.failure
import cl.hardroid.meliexamen.extensions.observe
import cl.hardroid.meliexamen.extensions.viewModel
import cl.hardroid.meliexamen.presentation.base.BaseActivity
import cl.hardroid.meliexamen.presentation.installments.InstallmentsActivity
import cl.hardroid.meliexamen.presentation.paymentmethod.PaymentMethodActivity
import kotlinx.android.synthetic.main.activity_card_issuers.*
import javax.inject.Inject

class CardIssuersActivity @Inject constructor() : BaseActivity(), CardIssuersNavigator {


    private lateinit var viewModel: CardIssuersViewModel

    override fun layoutId() = R.layout.activity_card_issuers
    lateinit var idPaymentMethod: String
    var amount: Int = 0

    lateinit var cardIssuersAdapter: CardIssuersAdapter

    companion object {

        val KEY_ID_CARD_ISSUERS = "key_id_card_issuers"

        fun newIntent(context: Context, amount: Int?, paymentMethodId: String?): Intent {
            val i = Intent(context, CardIssuersActivity::class.java)
            i.putExtra(PaymentMethodActivity.KEY_ID_AMMOUNT, amount)
            i.putExtra(PaymentMethodActivity.KEY_ID_PAYMENT, paymentMethodId)
            return i
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        idPaymentMethod = intent.getStringExtra(PaymentMethodActivity.KEY_ID_PAYMENT)
        amount = intent.getIntExtra(PaymentMethodActivity.KEY_ID_AMMOUNT, 0)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CardIssuersViewModel::class.java)

        viewModel = viewModel(viewModelFactory) {
            observe(viewModel.getCardIssuerd(), ::handleCardIssuers)
            failure(viewModel.failure, ::handleError)
        }
        rvListarTarjetasDisponibles.layoutManager = LinearLayoutManager(this)
        viewModel.loadCardIssuerd(idPaymentMethod)
    }

    private fun handleError(failure: Failure?) {
        Snackbar.make(
                rvListarTarjetasDisponibles,
                "Error {${failure.toString()}}",
                Snackbar.LENGTH_LONG).show()


    }

    private fun handleCardIssuers(list: List<CardIssuersDataModel>?) {
        cardIssuersAdapter = CardIssuersAdapter()
        cardIssuersAdapter.listCardIssuersDataModel = list!!
        cardIssuersAdapter.navigator = this
        rvListarTarjetasDisponibles.adapter = cardIssuersAdapter
    }

    override fun goToInstallments(idMedioDePago: CardIssuersDataModel?) {
        val i = InstallmentsActivity.newIntent(context, amount, idPaymentMethod, idMedioDePago!!.id)
        startActivity(i)
    }


}
