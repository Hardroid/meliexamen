package cl.hardroid.meliexamen.presentation.paymentmethod

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import cl.hardroid.meliexamen.R
import cl.hardroid.meliexamen.extensions.observe
import cl.hardroid.meliexamen.extensions.viewModel
import cl.hardroid.meliexamen.presentation.base.BaseActivity
import cl.hardroid.meliexamen.presentation.cardissuers.CardIssuersActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_medio_de_pago.*
import javax.inject.Inject

class PaymentMethodActivity @Inject constructor() : BaseActivity(), PaymentMethodNavigator {

    private lateinit var viewModel: PaymentMethodViewModel

    lateinit var paymentMethodAdapter: PaymentMethodAdapter
    lateinit var paymentMethodDataModel: PaymentMethodDataModel


    override fun layoutId() = R.layout.activity_medio_de_pago

    companion object {
        val KEY_ID_PAYMENT = "key_id_method_payment"
        val KEY_ID_AMMOUNT = "key_id_ammount"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PaymentMethodViewModel::class.java)

        viewModel = viewModel(viewModelFactory) {
            observe(viewModel.getPaymentMethod(), ::handlePaymentMethod)
        }

        rvListarMediosDePago.layoutManager = LinearLayoutManager(this)

        viewModel.loadPaymentMethodDataModel()
    }

    private fun handlePaymentMethod(list: List<PaymentMethodDataModel>?) {
        paymentMethodAdapter = PaymentMethodAdapter()
        paymentMethodAdapter.navigator = this
        paymentMethodAdapter.listMedios = list!!

        rvListarMediosDePago.adapter = paymentMethodAdapter
    }

    override fun goToSeleccionarBanco(idMedioDePago: PaymentMethodDataModel?) {
        this.paymentMethodDataModel = idMedioDePago!!
        Glide.with(this).load(paymentMethodDataModel.thumbnail).into(imgThumbnailSelected)
        btnAceptarMonto.setOnClickListener {
            if (!etMonto.text.toString().isEmpty()) {
                intent = CardIssuersActivity.newIntent(context, etMonto.text.toString().toInt(), idMedioDePago.id)
                startActivity(intent)
            } else {
                etMonto.error = "Ingrese monto"
            }
        }
    }


}