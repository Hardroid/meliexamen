package cl.hardroid.meliexamen.presentation.base

import android.app.Activity
import android.content.Context
import android.os.Bundle
import cl.hardroid.meliexamen.MeliApplication
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var context: Context

    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    abstract fun layoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        setSupportActionBar(toolbar)
    }

}

fun Activity.app() = applicationContext as MeliApplication