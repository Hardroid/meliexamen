package cl.hardroid.meliexamen.presentation.installments


data class IssuerDataModel(
        val id: String,
        val name: String,
        val secureThumbnail: String?,
        val thumbnail: String?
)