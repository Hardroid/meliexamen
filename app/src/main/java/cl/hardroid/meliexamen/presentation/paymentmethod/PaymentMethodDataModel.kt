package cl.hardroid.meliexamen.presentation.paymentmethod

class PaymentMethodDataModel(
        val id: String?,
        val name: String?,
        val paymentTypeId: String?,
        val status: String?,
        val secureThumbnail: String?,
        val thumbnail: String?,
        val deferredCapture: String?
)