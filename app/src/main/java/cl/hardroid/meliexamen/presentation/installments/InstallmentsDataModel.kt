package cl.hardroid.meliexamen.presentation.installments


data class InstallmentsDataModel(
        val paymentMethodId: String,
        val paymentTypeId: String,
        val issuer: IssuerDataModel?,
        val processingMode: String?,
        val merchantAccountId: String?,
        val payerCosts: List<PayerCostsDataModel>?
)