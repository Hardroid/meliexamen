package cl.hardroid.meliexamen.presentation.installments

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import cl.hardroid.meliexamen.R
import cl.hardroid.meliexamen.extensions.inflate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_installment.view.*
import kotlinx.android.synthetic.main.row_payment_method.view.*

class InstallmentsAdapter : RecyclerView.Adapter<InstallmentsAdapter.MedioDePagoViewHolder>() {

    lateinit var listPayerCostsDataModel: List<PayerCostsDataModel>
    lateinit var navigator: InstallmentsNavigator

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            MedioDePagoViewHolder(parent.inflate(R.layout.row_installment))

    override fun getItemCount() = listPayerCostsDataModel.size

    override fun onBindViewHolder(holder: MedioDePagoViewHolder, position: Int) =
            holder.bind(listPayerCostsDataModel[position], navigator)

    class MedioDePagoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(payerCostsDataModel: PayerCostsDataModel, navigator: InstallmentsNavigator) {
            itemView.txtCuotasDescripcion.text = payerCostsDataModel.recommendedMessage
            itemView.cardCuotas.setOnClickListener {
                navigator.goToPayerCost(payerCostsDataModel)
            }
        }
    }
}