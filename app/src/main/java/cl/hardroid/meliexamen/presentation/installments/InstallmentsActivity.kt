package cl.hardroid.meliexamen.presentation.installments

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import cl.hardroid.meliexamen.R
import cl.hardroid.meliexamen.R.id.rvListarTarjetasDisponibles
import cl.hardroid.meliexamen.domain.base.Failure
import cl.hardroid.meliexamen.extensions.failure
import cl.hardroid.meliexamen.extensions.observe
import cl.hardroid.meliexamen.extensions.viewModel
import cl.hardroid.meliexamen.presentation.base.BaseActivity
import cl.hardroid.meliexamen.presentation.cardissuers.CardIssuersActivity
import cl.hardroid.meliexamen.presentation.cardissuers.CardIssuersAdapter
import cl.hardroid.meliexamen.presentation.paymentmethod.PaymentMethodActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_card_issuers.*
import kotlinx.android.synthetic.main.activity_installments.*
import javax.inject.Inject

class InstallmentsActivity @Inject constructor() : BaseActivity(), InstallmentsNavigator {

    private lateinit var viewModel: InstallmentsViewModel

    override fun layoutId() = R.layout.activity_installments
    lateinit var idPaymentMethod: String
    lateinit var idCardIssuers: String
    var amount: Int = 0

    lateinit var installmentAdapter: InstallmentsAdapter

    companion object {


        fun newIntent(context: Context?, amount: Int?, paymentMethodId: String?, cardIssuersId: String?): Intent {
            val i = Intent(context, InstallmentsActivity::class.java)
            i.putExtra(PaymentMethodActivity.KEY_ID_AMMOUNT, amount)
            i.putExtra(PaymentMethodActivity.KEY_ID_PAYMENT, paymentMethodId)
            i.putExtra(CardIssuersActivity.KEY_ID_CARD_ISSUERS, cardIssuersId)
            return i
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        idPaymentMethod = intent.getStringExtra(PaymentMethodActivity.KEY_ID_PAYMENT)
        amount = intent.getIntExtra(PaymentMethodActivity.KEY_ID_AMMOUNT, 5000)
        idCardIssuers = intent.getStringExtra(CardIssuersActivity.KEY_ID_CARD_ISSUERS)


        viewModel = ViewModelProviders.of(this, viewModelFactory).get(InstallmentsViewModel::class.java)

        viewModel = viewModel(viewModelFactory) {
            observe(viewModel.getInstallment(), ::handleIntallment)
            failure(viewModel.failure, ::handleError)
        }

        viewModel.loadPaymentMethodDataModel(idPaymentMethod, idCardIssuers, amount)

        rvListarCuotas.layoutManager = LinearLayoutManager(this)
    }

    private fun handleIntallment(installmentsDataModel: InstallmentsDataModel?) {
        txtNombre.text = installmentsDataModel!!.issuer!!.name
        Glide.with(imgThumbnail.context).load(installmentsDataModel.issuer!!.thumbnail).into(imgThumbnail)

        installmentAdapter = InstallmentsAdapter()
        installmentAdapter.listPayerCostsDataModel = installmentsDataModel!!.payerCosts!!
        installmentAdapter.navigator = this
        rvListarCuotas.adapter = installmentAdapter
    }

    private fun handleError(failure: Failure?) {
        Snackbar.make(
                rvListarCuotas,
                "Error {${failure.toString()}}",
                Snackbar.LENGTH_LONG).show()


    }

    override fun goToPayerCost(payerCostsDataModel: PayerCostsDataModel) {
        Snackbar.make(
                rvListarCuotas,
                "${payerCostsDataModel.recommendedMessage}",
                Snackbar.LENGTH_LONG).show()
    }




}
