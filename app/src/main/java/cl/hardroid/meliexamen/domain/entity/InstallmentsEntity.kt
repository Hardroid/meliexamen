package cl.hardroid.meliexamen.domain.entity


data class InstallmentsEntity(
        val paymentMethodId: String,
        val paymentTypeId: String,
        val issuer: IssuerEntity?,
        val processingMode: String?,
        val merchantAccountId: String?,
        val payerCosts: List<PayerCostsEntity>?
)