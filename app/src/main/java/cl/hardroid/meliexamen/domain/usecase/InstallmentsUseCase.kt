package cl.hardroid.meliexamen.domain.usecase

import cl.hardroid.meliexamen.domain.base.UseCase
import cl.hardroid.meliexamen.domain.entity.CardIssuersEntity
import cl.hardroid.meliexamen.domain.entity.InstallmentsEntity
import cl.hardroid.meliexamen.domain.entity.PaymentMethodEntity
import cl.hardroid.meliexamen.domain.repository.PaymentMethodRepository
import javax.inject.Inject

class InstallmentsUseCase @Inject constructor(
        private val medioDePagoRepository: PaymentMethodRepository
) : UseCase<List<InstallmentsEntity>, InstallmentsUseCase.Params>() {

    override suspend fun run(params: InstallmentsUseCase.Params) =
            medioDePagoRepository.obtainPaymentMethodsInstallments(
                    params.ammount,
                    params.paymentId,
                    params.cardIssuerId
            )

    data class Params(
            val paymentId: String,
            val cardIssuerId: String,
            val ammount: Int
    )
}