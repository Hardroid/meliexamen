package cl.hardroid.meliexamen.domain.usecase

import cl.hardroid.meliexamen.domain.base.UseCase
import cl.hardroid.meliexamen.domain.entity.PaymentMethodEntity
import cl.hardroid.meliexamen.domain.repository.PaymentMethodRepository
import javax.inject.Inject

class PaymentMethodUseCase @Inject constructor(
        private val medioDePagoRepository: PaymentMethodRepository
) : UseCase<List<PaymentMethodEntity>, UseCase.None>() {

    override suspend fun run(params: UseCase.None) =
            medioDePagoRepository.obtainPaymentMethods()
}