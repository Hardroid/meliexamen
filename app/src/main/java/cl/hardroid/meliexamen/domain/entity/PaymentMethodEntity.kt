package cl.hardroid.meliexamen.domain.entity


data class PaymentMethodEntity(
        val id: String?,
        val name: String?,
        val paymentTypeId: String?,
        val status: String?,
        val secureThumbnail: String?,
        val thumbnail: String?,
        val deferredCapture: String?
)