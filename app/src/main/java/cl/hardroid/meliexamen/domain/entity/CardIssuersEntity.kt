package cl.hardroid.meliexamen.domain.entity


data class CardIssuersEntity(
        val id: String,
        val name: String,
        val secureThumbnail: String?,
        val thumbnail: String?,
        val processingMode: String?,
        val merchantAccountId: String?
)