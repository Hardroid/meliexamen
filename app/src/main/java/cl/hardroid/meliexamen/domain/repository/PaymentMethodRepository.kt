package cl.hardroid.meliexamen.domain.repository

import cl.hardroid.meliexamen.domain.base.Either
import cl.hardroid.meliexamen.domain.base.Failure
import cl.hardroid.meliexamen.domain.entity.CardIssuersEntity
import cl.hardroid.meliexamen.domain.entity.InstallmentsEntity
import cl.hardroid.meliexamen.domain.entity.PaymentMethodEntity

interface PaymentMethodRepository {

    fun obtainPaymentMethods(): Either<Failure, List<PaymentMethodEntity>>
    fun obtainPaymentMethodsCardIssuers(paymentMethodId: String?): Either<Failure, List<CardIssuersEntity>>
    fun obtainPaymentMethodsInstallments(
            amount: Int,
            paymentMethodId: String,
            issuerId: String
    ): Either<Failure, List<InstallmentsEntity>>
}