package cl.hardroid.meliexamen.domain.entity


data class IssuerEntity(
        val id: String,
        val name: String,
        val secureThumbnail: String?,
        val thumbnail: String?
)