package cl.hardroid.meliexamen.domain.usecase

import cl.hardroid.meliexamen.domain.base.UseCase
import cl.hardroid.meliexamen.domain.entity.CardIssuersEntity
import cl.hardroid.meliexamen.domain.entity.PaymentMethodEntity
import cl.hardroid.meliexamen.domain.repository.PaymentMethodRepository
import javax.inject.Inject

class CardIssuersUseCase @Inject constructor(
        private val medioDePagoRepository: PaymentMethodRepository
) : UseCase<List<CardIssuersEntity>, CardIssuersUseCase.Params>() {

    override suspend fun run(params: CardIssuersUseCase.Params) =
            medioDePagoRepository.obtainPaymentMethodsCardIssuers(params.id)

    data class Params(val id: String?)
}