package cl.hardroid.meliexamen.data.remote.utils

import android.content.Context
import cl.hardroid.meliexamen.extensions.networkInfo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkHandler @Inject constructor(private val context: Context) {
    val isConnected get() = context.networkInfo?.isConnected
}