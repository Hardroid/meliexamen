package cl.hardroid.meliexamen.data.remote

import javax.inject.Inject

class MeliService @Inject constructor() : MeliServiceApi {

    @Inject
    lateinit var meliService: MeliServiceApi

    override fun getPaymentMethods(publicKey: String?) = meliService.getPaymentMethods()

    override fun getPaymentMethodsCardIssuers(
            publicKey: String?,
            paymentMethodId: String?) = meliService.getPaymentMethodsCardIssuers(paymentMethodId = paymentMethodId)

    override fun getPaymentMethodsInstallments(
            publicKey: String?,
            amount: Int?,
            paymentMethodId: String?,
            issuerId: String?) = meliService.getPaymentMethodsInstallments(
            amount = amount,
            paymentMethodId = paymentMethodId,
            issuerId = issuerId
    )
}