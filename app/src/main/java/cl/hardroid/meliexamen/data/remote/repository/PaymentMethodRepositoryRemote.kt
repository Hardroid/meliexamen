package cl.hardroid.meliexamen.data.remote.repository

import cl.hardroid.meliexamen.data.remote.MeliService
import cl.hardroid.meliexamen.data.remote.MeliServiceApi
import cl.hardroid.meliexamen.data.remote.model.InstallmentsModel
import cl.hardroid.meliexamen.data.remote.utils.NetworkHandler
import cl.hardroid.meliexamen.domain.base.Either
import cl.hardroid.meliexamen.domain.base.Failure
import cl.hardroid.meliexamen.domain.entity.CardIssuersEntity
import cl.hardroid.meliexamen.domain.entity.InstallmentsEntity
import cl.hardroid.meliexamen.domain.entity.PaymentMethodEntity
import cl.hardroid.meliexamen.domain.repository.PaymentMethodRepository
import cl.hardroid.meliexamen.extensions.emptyObject
import retrofit2.Call
import javax.inject.Inject

class PaymentMethodRepositoryRemote @Inject constructor(private val networkHandler: NetworkHandler,
                                                        private val service: MeliServiceApi) : PaymentMethodRepository {

    override fun obtainPaymentMethods(): Either<Failure, List<PaymentMethodEntity>> {
        return when (networkHandler.isConnected) {
            true -> request(service.getPaymentMethods(), { it.map { it.toPaymentMethodEntity() } }, emptyList())
            false, null -> Either.Left(Failure.NetworkConnection())
        }
    }


    override fun obtainPaymentMethodsCardIssuers(paymentMethodId: String?): Either<Failure, List<CardIssuersEntity>> {
        return when (networkHandler.isConnected) {
            true -> request(service.getPaymentMethodsCardIssuers(paymentMethodId = paymentMethodId), {
                it.map { it.toCardIssuersEntity() }
            }, emptyList()
            )
            false, null -> Either.Left(Failure.NetworkConnection())
        }
    }

    override fun obtainPaymentMethodsInstallments(amount: Int, paymentMethodId: String, issuerId: String): Either<Failure, List<InstallmentsEntity>> {
        return when (networkHandler.isConnected) {
            true -> request(service.getPaymentMethodsInstallments(
                    amount = amount,
                    issuerId = issuerId,
                    paymentMethodId = paymentMethodId), { it.map { it.toInstallmentsEntity() } }, emptyList())
            false, null -> Either.Left(Failure.NetworkConnection())
        }
    }

    private fun <T, R> request(call: Call<T>, transform: (T) -> R, default: T): Either<Failure, R> {
        return try {
            val response = call.execute()
            when (response.isSuccessful) {
                true -> Either.Right(transform((response.body() ?: default)))
                false -> Either.Left(Failure.ServerError())
            }
        } catch (exception: Throwable) {
            Either.Left(Failure.ServerError())
        }
    }
}