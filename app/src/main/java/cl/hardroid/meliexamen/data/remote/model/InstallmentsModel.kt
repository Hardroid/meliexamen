package cl.hardroid.meliexamen.data.remote.model

import cl.hardroid.meliexamen.domain.entity.InstallmentsEntity
import com.google.gson.annotations.SerializedName

data class InstallmentsModel(
        @SerializedName("payment_method_id")
        val paymentMethodId: String,
        @SerializedName("payment_type_id")
        val paymentTypeId: String,
        val issuer: IssuerModel?,
        @SerializedName("processing_mode")
        val processingMode: String?,
        @SerializedName("merchant_account_id")
        val merchantAccountId: String?,
        @SerializedName("payer_costs")
        val payerCosts: List<PayerCostsModel>?
) {
    companion object
    fun toInstallmentsEntity() = InstallmentsEntity(
            paymentMethodId,
            paymentTypeId,
            issuer!!.toCardIssuersEntity(),
            processingMode,
            merchantAccountId,
            payerCosts!!.map {
                it.toPayerCostsEntity()
            })
}