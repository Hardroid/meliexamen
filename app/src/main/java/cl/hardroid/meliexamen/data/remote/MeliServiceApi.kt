package cl.hardroid.meliexamen.data.remote

import cl.hardroid.meliexamen.data.remote.model.CardIssuersModel
import cl.hardroid.meliexamen.data.remote.model.InstallmentsModel
import cl.hardroid.meliexamen.data.remote.model.PaymentMethodModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MeliServiceApi {

    companion object {
        private const val PAYMENT_METHODS = "payment_methods"
        private const val CARD_ISSUERS = "$PAYMENT_METHODS/card_issuers"
        private const val INSTALLMENTS = "$PAYMENT_METHODS/installments"

        private const val PARAM_PUBLIC_KEY = "public_key"
        private const val PARAM_PUBLIC_KEY_VALUE = "444a9ef5-8a6b-429f-abdf-587639155d88"
        private const val PARAM_PAYMENT_METHOD_ID = "payment_method_id"
        private const val PARAM_AMOUNT = "amount"
        private const val ISSUER_ID = "issuer.id"
    }

    @GET(PAYMENT_METHODS)
    fun getPaymentMethods(
            @Query(PARAM_PUBLIC_KEY) publicKey: String? = PARAM_PUBLIC_KEY_VALUE
    )
            : Call<List<PaymentMethodModel>>

    @GET(CARD_ISSUERS)
    fun getPaymentMethodsCardIssuers(
            @Query(PARAM_PUBLIC_KEY) publicKey: String? = PARAM_PUBLIC_KEY_VALUE,
            @Query(PARAM_PAYMENT_METHOD_ID) paymentMethodId: String?
    )
            : Call<List<CardIssuersModel>>

    @GET(INSTALLMENTS)
    fun getPaymentMethodsInstallments(
            @Query(PARAM_PUBLIC_KEY) publicKey: String? = PARAM_PUBLIC_KEY_VALUE,
            @Query(PARAM_AMOUNT) amount: Int?,
            @Query(PARAM_PAYMENT_METHOD_ID) paymentMethodId: String?,
            @Query(ISSUER_ID) issuerId: String?)
            : Call<List<InstallmentsModel>>
}