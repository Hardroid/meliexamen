package cl.hardroid.meliexamen.data.remote.model

import cl.hardroid.meliexamen.domain.entity.PaymentMethodEntity
import com.google.gson.annotations.SerializedName

data class PaymentMethodModel(
        val id: String,
        val name: String,
        @SerializedName("payment_type_id")
        val paymentTypeId: String?,
        val status: String?,
        @SerializedName("secure_thumbnail")
        val secureThumbnail: String?,
        val thumbnail: String?,
        @SerializedName("deferred_capture")
        val deferredCapture: String?
) {
    fun toPaymentMethodEntity() = PaymentMethodEntity(
            id,
            name,
            paymentTypeId,
            status,
            secureThumbnail,
            thumbnail,
            deferredCapture
    )
}