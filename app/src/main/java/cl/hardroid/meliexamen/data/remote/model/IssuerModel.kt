package cl.hardroid.meliexamen.data.remote.model

import cl.hardroid.meliexamen.domain.entity.IssuerEntity
import com.google.gson.annotations.SerializedName

data class IssuerModel(
        val id: String,
        val name: String,
        @SerializedName("secure_thumbnail")
        val secureThumbnail: String?,
        val thumbnail: String?
) {
    fun toCardIssuersEntity() = IssuerEntity(
            id,
            name,
            secureThumbnail,
            thumbnail
    )
}