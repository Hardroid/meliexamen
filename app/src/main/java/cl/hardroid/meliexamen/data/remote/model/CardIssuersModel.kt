package cl.hardroid.meliexamen.data.remote.model

import cl.hardroid.meliexamen.domain.entity.CardIssuersEntity
import com.google.gson.annotations.SerializedName

//@JsonClass(generateAdapter = true)
data class CardIssuersModel(
        val id: String,
        val name: String,
        @SerializedName("secure_thumbnail")
        val secureThumbnail: String?,
        val thumbnail: String?,
        @SerializedName("processing_mode")
        val processingMode: String?,
        @SerializedName("merchant_account_id")
        val merchantAccountId: String?
){
    fun toCardIssuersEntity() = CardIssuersEntity(
            id,
            name,
            secureThumbnail,
            thumbnail,
            processingMode,
            merchantAccountId
    )
}