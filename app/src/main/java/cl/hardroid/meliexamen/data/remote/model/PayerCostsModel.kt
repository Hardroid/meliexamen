package cl.hardroid.meliexamen.data.remote.model

import cl.hardroid.meliexamen.domain.entity.PayerCostsEntity
import com.google.gson.annotations.SerializedName

data class PayerCostsModel(
        val installments: Int?,
        @SerializedName("installment_rate")
        val installmentRate: Float?,
        @SerializedName("discount_rate")
        val discountRate: Int?,
        val labels: List<String>?,
        @SerializedName("installment_rate_collector")
        val installmentRateCollector: List<String>?,
        @SerializedName("min_allowed_amount")
        val minAllowedAmount: Int?,
        @SerializedName("max_allowed_amount")
        val maxAllowedAmount: Int?,
        @SerializedName("recommended_message")
        val recommendedMessage: String?,
        @SerializedName("installment_amount")
        val installmentAmount: Float?,
        @SerializedName("total_amount")
        val totalAmount: Float?
) {
    fun toPayerCostsEntity() = PayerCostsEntity(
            installments,
            installmentRate,
            discountRate,
            labels,
            installmentRateCollector,
            minAllowedAmount,
            maxAllowedAmount,
            recommendedMessage,
            installmentAmount,
            totalAmount
    )
}