package cl.hardroid.meliexamen.di

import cl.hardroid.meliexamen.presentation.cardissuers.CardIssuersActivity
import cl.hardroid.meliexamen.presentation.installments.InstallmentsActivity
import cl.hardroid.meliexamen.presentation.paymentmethod.PaymentMethodActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class UiModule{
    @ContributesAndroidInjector
    abstract fun contributesMedioDePagoActivity(): PaymentMethodActivity

    @ContributesAndroidInjector
    abstract fun contributesCardIssuers(): CardIssuersActivity

    @ContributesAndroidInjector
    abstract fun contributesInstallments(): InstallmentsActivity

}
