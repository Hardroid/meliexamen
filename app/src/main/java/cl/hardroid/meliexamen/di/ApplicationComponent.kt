package cl.hardroid.meliexamen.di

import android.app.Application
import cl.hardroid.meliexamen.MeliApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = arrayOf(
                AndroidInjectionModule::class,
                AndroidSupportInjectionModule::class,
                ApplicationModule::class,
                RemoteModule::class,
                ViewModelModule::class,
                UiModule::class,
                DataModule::class
        )
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: MeliApplication)
}