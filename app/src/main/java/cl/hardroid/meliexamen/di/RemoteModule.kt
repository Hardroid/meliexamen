package cl.hardroid.meliexamen.di

import cl.hardroid.meliexamen.BuildConfig
import cl.hardroid.meliexamen.data.remote.MeliServiceApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
object RemoteModule {

    @JvmStatic
    @Singleton
    @Provides
    fun provideMeliService(retrofit: Retrofit): MeliServiceApi =
            retrofit.create(MeliServiceApi::class.java)

    @JvmStatic
    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://api.mercadopago.com/v1/")
                .client(makeOkHttpClient(makeLoggingInterceptor(BuildConfig.DEBUG)))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    private fun makeLoggingInterceptor(isDebug: Boolean): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (isDebug) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
        return logging
    }

    private fun makeOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor) = OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .build()
}