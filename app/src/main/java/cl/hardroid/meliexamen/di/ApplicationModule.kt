package cl.hardroid.meliexamen.di

import android.app.Application
import android.content.Context
import cl.hardroid.meliexamen.MeliApplication
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule(private val application: MeliApplication){
    @Binds
    abstract fun bindContext(application: Application): Context

}