package cl.hardroid.meliexamen.di

import cl.hardroid.meliexamen.data.remote.repository.PaymentMethodRepositoryRemote
import cl.hardroid.meliexamen.domain.repository.PaymentMethodRepository
import dagger.Binds
import dagger.Module

@Module
abstract class DataModule {

    @Binds
    abstract fun bindMedioDePagoRepository(dataRepository: PaymentMethodRepositoryRemote)
            : PaymentMethodRepository
}