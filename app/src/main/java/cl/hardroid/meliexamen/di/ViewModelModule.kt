package cl.hardroid.meliexamen.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import cl.hardroid.meliexamen.presentation.base.ViewModelFactory
import cl.hardroid.meliexamen.presentation.cardissuers.CardIssuersViewModel
import cl.hardroid.meliexamen.presentation.installments.InstallmentsViewModel
import cl.hardroid.meliexamen.presentation.paymentmethod.PaymentMethodViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class ViewModelModule{

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(PaymentMethodViewModel::class)
    abstract fun bindSeleccionarMedioDePagoViewModel(viewModel: PaymentMethodViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CardIssuersViewModel::class)
    abstract fun bindSeleccionarCardIssuersViewModel(viewModel: CardIssuersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InstallmentsViewModel::class)
    abstract fun bindSeleccionarInstallmentsViewModel(viewModel: InstallmentsViewModel): ViewModel
}
@MapKey
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)