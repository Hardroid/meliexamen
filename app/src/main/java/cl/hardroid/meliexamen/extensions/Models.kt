package cl.hardroid.meliexamen.extensions

import cl.hardroid.meliexamen.data.remote.model.InstallmentsModel


fun InstallmentsModel.Companion.emptyObject() = InstallmentsModel(
            "",
            "",
            null,
            "",
            "",
            emptyList())
