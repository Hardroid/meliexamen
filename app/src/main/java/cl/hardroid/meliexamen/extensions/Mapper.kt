package cl.hardroid.meliexamen.extensions

import cl.hardroid.meliexamen.domain.entity.*
import cl.hardroid.meliexamen.presentation.cardissuers.CardIssuersDataModel
import cl.hardroid.meliexamen.presentation.installments.InstallmentsDataModel
import cl.hardroid.meliexamen.presentation.installments.IssuerDataModel
import cl.hardroid.meliexamen.presentation.installments.PayerCostsDataModel
import cl.hardroid.meliexamen.presentation.paymentmethod.PaymentMethodDataModel

fun PaymentMethodEntity.toViewDataModel(
        paymentMethodEntity: PaymentMethodEntity
) = PaymentMethodDataModel(
        paymentMethodEntity.id,
        paymentMethodEntity.name,
        paymentMethodEntity.paymentTypeId,
        paymentMethodEntity.status,
        paymentMethodEntity.secureThumbnail,
        paymentMethodEntity.thumbnail,
        paymentMethodEntity.deferredCapture)


fun CardIssuersEntity.toViewDataModel(
        cardIssuersEntity: CardIssuersEntity
) = CardIssuersDataModel(
        cardIssuersEntity.id,
        cardIssuersEntity.name,
        cardIssuersEntity.secureThumbnail,
        cardIssuersEntity.thumbnail,
        cardIssuersEntity.processingMode,
        cardIssuersEntity.merchantAccountId)

fun IssuerEntity.toViewDataModel(
        issuerEntity: IssuerEntity
) = IssuerDataModel(
        issuerEntity.id,
        issuerEntity.name,
        issuerEntity.secureThumbnail,
        issuerEntity.thumbnail)

fun PayerCostsEntity.toViewDataModel(
        payerCostsEntity: PayerCostsEntity
) = PayerCostsDataModel(
        payerCostsEntity.installments,
        payerCostsEntity.installmentRate,
        payerCostsEntity.discountRate,
        payerCostsEntity.labels,
        payerCostsEntity.installmentRateCollector,
        payerCostsEntity.minAllowedAmount,
        payerCostsEntity.maxAllowedAmount,
        payerCostsEntity.recommendedMessage,
        payerCostsEntity.installmentAmount,
        payerCostsEntity.totalAmount
)

fun InstallmentsEntity.toViewDataModel(
        installmentsEntity: InstallmentsEntity
) = InstallmentsDataModel(
        installmentsEntity.paymentMethodId,
        installmentsEntity.paymentTypeId,
        installmentsEntity.issuer!!.toViewDataModel(installmentsEntity.issuer!!),
        installmentsEntity.processingMode,
        installmentsEntity.merchantAccountId,
        installmentsEntity.payerCosts!!.map {
            it.toViewDataModel(it)
        })


